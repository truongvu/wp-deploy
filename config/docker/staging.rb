# The WordPress 'Site Title' for the website
set :wp_sitename, 'Mecox'

# The URL of the website in this environment.
set :stage_url, 'http://labs.codeenginestudio.com/mecox'

#Repo to deploy
set :repo_url, 'git@gitlab.com:truongvu/wp-deploy.git'

# The deploy path to the website on this environment's server.
set :deploy_to, '/home/dfjutsvt/labs.codeenginestudio.com/mecox'
set :tmp_dir, "/home/dfjutsvt/labs.codeenginestudio.com/mecox/tmp"

# The Git branch this environment should be attached to.
set :branch, 'master'

# The environment's name. To be used in commands and other references.
set :stage, :staging

# The environment's server credentials
server '209.58.176.1', port: 27880, user: 'dfjutsvt', roles: %w(web app db)

# Config WP
# The WordPress admin user
set :wp_user, 'admin'

# The WordPress admin email address
set :wp_email, 'dev@codeenginestudio.com'

# Extra command
SSHKit.config.command_map[:wp] = "/home/dfjutsvt/bin/wp"

SSHKit.config.command_map[:make_htaccess] = "/home/dfjutsvt/bin/make_htaccess"

# Run command to create htaccess file
on roles(:app) do
 execute :make_htaccess, "#{deploy_to}"
end