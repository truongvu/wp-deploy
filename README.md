//////////////////////////////////////////////////////////////////////
// Secret Variable: DEPLOY_CONFIG
//////////////////////////////////////////////////////////////////////

```ruby
# The Git branch this environment should be attached to.
set :branch, 'master'

# The environment's name. To be used in commands and other references.
set :stage, :staging

# The URL of the website in this environment.
set :stage_url, 'http://labs.codeenginestudio.com/mecox'

# The environment's server credentials
server '209.58.176.1', port: 27880, user: 'dfjutsvt', roles: %w(web app db)

# The deploy path to the website on this environment's server.
set :deploy_to, '~/labs.codeenginestudio.com/mecox'

set :tmp_dir, "~/labs.codeenginestudio.com/mecox/tmp"
```

//////////////////////////////////////////////////////////////////////
// Secret Variable: DB_CONFIG
//////////////////////////////////////////////////////////////////////
```
staging:
  host: '209.58.176.1'
  database: dfjutsvt_mecox
  username: dfjutsvt_labs
  password: 'n!~u+]s_]5&_'

```